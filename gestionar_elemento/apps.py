from django.apps import AppConfig


class GestionarElementoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gestionar_elemento'
